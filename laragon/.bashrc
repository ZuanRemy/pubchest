# ~/.bashrc
# This come from bash on cmder installed from laragon
# 

# CONSOLE - change (~) env path to USER_DIR
HOME=$USER_DIR

# USER - Shorthand
alias clear='cls'
alias ls='ls -a --show-control-chars -F --color $*'
alias ll='ls -gohlat --show-control-chars -F --color $*'

# USER - Dir Shortcut
alias cdl='cd "$LARAGON_ROOT"'
alias cdc='cd "$CMDER_ROOT\\config"'
alias cdu='cd "$USR_DIR"'
alias cdh='cd "C:\\Users\\Lappy"'

# SSH - Add all SSH RSA Private Key available to SSH Agent for easy auth
alias sshx='eval `ssh-agent -s` && ssh-add ~/.ssh/*_rsa'

alias history='cat "$CMDER_ROOT\\config\\.history"'
alias wget='curl -OL $*'
alias nodejs='node $*'
alias gl='git log --oneline --all --graph --decorate  $*'
alias npp='D:\\work\\tools\\npp.7.6.6\\notepad++.exe $*'

alias e='D:\\work\\tools\\npp.7.6.6\\notepad++.exe $*'
alias o='explorer $*'
alias o.='explorer .'




